/**

 * Copyright (c) 2013 Samsung Electronics Co., Ltd.

 * All rights reserved.

 *

 * This software is a confidential and proprietary information of Samsung

 * Electronics, Inc. ("Confidential Information").  You shall not disclose such

 * Confidential Information and shall use it only in accordance with the terms

 * of the license agreement you entered into with Samsung Electronics.

 */

package com.sec.activemode;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.accessibility.AccessibilityManager;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.sec.activemode.barometer.BarometerConfig;
import com.sec.activemode.barometer.BarometerFragment;
import com.sec.activemode.barometer.BarometerSettingsActivity;
import com.sec.activemode.barometer.data.BarometerConstants;
import com.sec.activemode.compass.CompassConfig;
import com.sec.activemode.compass.data.CompassConstants;
import com.sec.activemode.compass.ui.CompassFragment;
import com.sec.activemode.compass.ui.CompassPreferenceActivity;
import com.sec.activemode.flashlight.FlashSettingsActivity;
import com.sec.activemode.flashlight.TorchFragment;
import com.sec.activemode.lib.ActiveModeFragment;
import com.sec.activemode.lib.ActivityMateProvider;
import com.sec.activemode.lib.AppInfo;
import com.sec.activemode.lib.Lib;
import com.sec.activemode.stopwatch.Stopwatch;
import com.sec.android.emergencymode.EmergencyConstants;

public class LauncherActivity extends FragmentActivity implements
        OnClickListener, Callback /* , OnSharedPreferenceChangeListener */{
    private static final String TAG = "LauncherActivity";

    public static final String FROM_LAUNCHER_ACTIVITY = "from_launcher_activity";

    private static final String CURRENTFRAGMENT_KEY = "current_fragment";

    private static final String DRAWERISOPEN_KEY = "drawer_is_open";

    private static final String BAROMETER_KEY = "barometer_save_key";

    private static final String COMPASS_KEY = "compass_save_key";

    private static final String FLASHLIGHT_KEY = "flashlight_save_key";

    private static final String STOPWATCH_KEY = "stopwatch_save_key";

    public static int NUM_DEFAULT_APPS = 7;

    public static final int NUM_DEFAULT_FIXED_APPS = 6;

    private static final int REQUEST_ADD = 100;

    private static final int REQUEST_REORDER = 101;

    private static final int REQUEST_TERMS_CONDITIONS = 102;

    public static final int REQUEST_CODE_TORCH = 103;

    private static final int REQUEST_CODE_SHEALTH = 104;

    public static final int FRAGMENT_POSITION_MAIN = -1;

    public static final int FRAGMENT_POSITION_BAROMETER = 0;

    public static final int FRAGMENT_POSITION_COMPASS = 1;

    public static final int FRAGMENT_POSITION_STOPWATCH = 3;

    public static final int FRAGMENT_POSITION_TORCH = 2;

    public final static int NOTIFICATION_ID = 130690;

    public static String mDrawerMenuContentDescription;

    public static ActivityMateProvider mActivityMateProvider;

    public static ArrayList<AppInfo> mSelectedApps;

    public static ArrayList<AppInfo> mRemoveApps = new ArrayList<AppInfo>();

    private static ArrayList<AppInfo> mDefaultApps;

    public static SelectedAppsAdapter mSelectedAppsAdapter;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    public ViewPager mViewPager;

    private LinearLayout mIndicatorLayout;

    private ImageView mImgIndicatorBaro;

    private ImageView mImgIndicatorCompass;

    private ImageView mImgIndicatorFlash;

    private ImageView mImgIndicatorStopwatch;

    private ArrayList<ActiveModeFragment> mListFragment;

    private DrawerLayout mMainLayout;

    private ActionBarDrawerToggle mDrawerToggle;

    private ListView mListView;

    private Button mBtnManageDrawer;

    private Context mContext;

    private MainFragment mMainFragment;

    private CompassFragment mCompassFragment;

    private BarometerFragment mBarometerFragment;

    private TorchFragment mTorchFragment;

    private Stopwatch mStopwatchFragment;

    public static int mCurrentFragment = FRAGMENT_POSITION_MAIN;

    public static final String KEY_PREF_TERMS_CONDITIONS = "key_pref_first_launch";

    private SharedPreferences mPrefs = null;

    public static boolean mIsOpeningDrawer;

    public static boolean mShowShortcuts = true;

    public static String[] mDefaultClassNames;

    public static Drawable[] mDefaultAppIcons;

    public static String[] mDefaultAppNames;

    public static SurfaceView mSurfaceView;

    private SurfaceHolder mSurfaceHolder;

    public static Camera mCamera;

    private Camera.Parameters mParameters;

    private Point mScreenResolution;

    private boolean mIsScrolling = false;

    private boolean isTheFirstLaunchSHealth = true;

    public boolean mIsActivityDestroyed = true;

    public static AccessibilityManager mAccessibilityManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        initActionBar();
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate " + Build.MODEL.toString());

        mContext = this.getApplicationContext();
        mAccessibilityManager = (AccessibilityManager)getSystemService(Context.ACCESSIBILITY_SERVICE);

        if (Build.MODEL.contains("SC-02G") && "DCM".equals(getSaleCode())) {
            NUM_DEFAULT_APPS = 9;
            initDefaultDcmAppInfo();
        } else {
            NUM_DEFAULT_APPS = 7;
            initDefaultAppInfo();
        }

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (mPrefs.getBoolean(KEY_PREF_TERMS_CONDITIONS, true)) {

            String countryCode = SystemProperties.get("ro.csc.country_code");

            if (!"USA".equalsIgnoreCase(countryCode)) {
                Editor editor = PreferenceManager.getDefaultSharedPreferences(
                        this).edit();
                editor.putString(
                        BarometerSettingsActivity.KEY_PREF_UNIT_HEIGHT, ""
                                + BarometerConstants.UNIT_HEIGHT_METER);
                editor.putString(
                        BarometerSettingsActivity.KEY_PREF_UNIT_PRESSURE, ""
                                + BarometerConstants.UNIT_PRESSURE_KPA);
                editor.commit();
            }

            Intent intent = new Intent(this, TermsAndConditions.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("first_launcher", true);
            startActivityForResult(intent, REQUEST_TERMS_CONDITIONS);
        }

        setContentView(R.layout.activitymate_launcher);

        loadDefaultAppsFromXML();

        loadSelectedAppsFromDB();

        CompassConfig.loadConfig(mContext);
        BarometerConfig.loadConfig(mContext);
        mMainLayout = (DrawerLayout) findViewById(R.id.activitymate_main_layout);
        mMainLayout.setDrawerShadow(R.color.main_drawer_shadow,
                GravityCompat.START);

        mDrawerToggle = new ActionBarDrawerToggle(this, mMainLayout,
                R.drawable.active_mode_drawer,
                R.string.ss_double_tap_to_open_t_tts,
                R.string.ss_double_tap_to_close_tts) {

            public void onDrawerClosed(View view) {

                final String toggleContentDesc = getResources().getString(
                        R.string.ss_drawer_menu_tts)
                        + ", "
                        + getResources().getString(
                                R.string.ss_double_tap_to_open_t_tts);
                getActionBar().setHomeActionContentDescription(
                        toggleContentDesc);
            }

            public void onDrawerOpened(View drawerView) {

                final String toggleContentDesc = getResources().getString(
                        R.string.ss_drawer_menu_tts)
                        + ", "
                        + getResources().getString(
                                R.string.ss_double_tap_to_close_tts);
                getActionBar().setHomeActionContentDescription(
                        toggleContentDesc);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

                super.onDrawerSlide(drawerView, slideOffset);

                if (slideOffset != 0) {
                    setActionBarColor(false, slideOffset);
                    mIsOpeningDrawer = true;
                    if (mTorchFragment != null) {
                        mTorchFragment.hideKeyboard();
                    }
                    invalidateOptionsMenu();
                } else {
                    setActionBarColor(true, slideOffset);
                    mIsOpeningDrawer = false;
                    invalidateOptionsMenu();
                }
            }
        };

        mMainLayout.setDrawerListener(mDrawerToggle);

        mListView = (ListView) findViewById(R.id.activitymate_drawer_list);
        mSelectedAppsAdapter = new SelectedAppsAdapter(this, mSelectedApps);
        mListView.setAdapter(mSelectedAppsAdapter);
        mListView.setOnItemClickListener(mOnItemClickListener);

        mBtnManageDrawer = (Button) findViewById(R.id.activitymate_reorder_button);
        mBtnManageDrawer.setOnClickListener(this);
        mIndicatorLayout = ((LinearLayout) findViewById(R.id.activitymate_indicator_layout));
        mListFragment = new ArrayList<ActiveModeFragment>();

        mViewPager = (ViewPager) findViewById(R.id.activitymate_viewpager);

        mImgIndicatorBaro = (ImageView) findViewById(R.id.activitymate_indicator1);
        mImgIndicatorCompass = (ImageView) findViewById(R.id.activitymate_indicator2);
        mImgIndicatorFlash = (ImageView) findViewById(R.id.activitymate_indicator3);
        mImgIndicatorStopwatch = (ImageView) findViewById(R.id.activitymate_indicator4);

        mViewPager.setOnPageChangeListener(mOnPagerChange);

        mSurfaceView = (SurfaceView) findViewById(R.id.compass_surface_view);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceView.setFixedOrientation(0x00010000);

        if (savedInstanceState != null) {

            mCurrentFragment = savedInstanceState.getInt(CURRENTFRAGMENT_KEY);

            mIsOpeningDrawer = savedInstanceState.getBoolean(DRAWERISOPEN_KEY);

            if (mCurrentFragment != FRAGMENT_POSITION_MAIN) {
                restoreFragments(savedInstanceState);
            }
        } else {
            mCurrentFragment = FRAGMENT_POSITION_MAIN;

            mIsOpeningDrawer = false;

        }
        if (mCurrentFragment == FRAGMENT_POSITION_MAIN) {

            switchtoMainFragment(false);

        } else {
            switchFragment(mCurrentFragment, false);

        }
        if (mIsOpeningDrawer) {

            setActionBarColor(false, 1);

        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        filter.addDataScheme("package");
        getApplicationContext().registerReceiver(mReceiverUninstall, filter);

    }

    public void initDefaultAppInfo() {

        mDefaultClassNames = new String[] { "com.sec.activemode.main",
                "com.sec.activemode.barometer", "com.sec.activemode.compass",
                "com.sec.activemode.torch", "com.sec.activemode.stopwatch",
                "com.sec.activemode.shortcuts", "com.sec.activemode.exercise" };

        mDefaultAppIcons = new Drawable[] {
                getResources().getDrawable(R.drawable.drawer_list_icon_main),
                getResources().getDrawable(
                        R.drawable.drawer_list_icon_barometer),
                getResources().getDrawable(R.drawable.drawer_list_icon_compass),
                getResources().getDrawable(R.drawable.drawer_list_icon_torch),
                getResources().getDrawable(
                        R.drawable.drawer_list_icon_stopwatch),
                getResources().getDrawable(
                        R.drawable.tw_expander_open_01_holo_light),
                getResources()
                        .getDrawable(R.drawable.drawer_list_icon_exercise) };

        mDefaultAppNames = new String[] {
                getResources().getString(R.string.ss_main_screen),
                getResources().getString(R.string.ss_barometer),
                getResources().getString(R.string.ss_compass),
                getResources().getString(R.string.ss_torch_light_abb),
                getResources().getString(R.string.ss_stopwatch),
                getResources().getString(R.string.ts_app_shortcuts),
                getResources().getString(R.string.ss_exercise_opt) };
    }

    public void initDefaultDcmAppInfo() {

        mDefaultClassNames = new String[] { "com.sec.activemode.main",
                "com.sec.activemode.barometer", "com.sec.activemode.compass",
                "com.sec.activemode.torch", "com.sec.activemode.stopwatch",
                "com.sec.activemode.shortcuts", "com.sec.activemode.exercise",
                "com.sec.activemode.emergency", "com.sec.activemode.geo" };

        mDefaultAppIcons = new Drawable[] {
                getResources().getDrawable(R.drawable.drawer_list_icon_main),
                getResources().getDrawable(
                        R.drawable.drawer_list_icon_barometer),
                getResources().getDrawable(R.drawable.drawer_list_icon_compass),
                getResources().getDrawable(R.drawable.drawer_list_icon_torch),
                getResources().getDrawable(
                        R.drawable.drawer_list_icon_stopwatch),
                getResources().getDrawable(
                        R.drawable.tw_expander_open_01_holo_light),
                getResources()
                        .getDrawable(R.drawable.drawer_list_icon_exercise),
                getResources().getDrawable(
                        R.drawable.drawer_list_icon_emergency_mode),
                getResources()
                        .getDrawable(R.drawable.drawer_list_icon_geo_news) };

        mDefaultAppNames = new String[] {
                getResources().getString(R.string.ss_main_screen),
                getResources().getString(R.string.ss_barometer),
                getResources().getString(R.string.ss_compass),
                getResources().getString(R.string.ss_torch_light_abb),
                getResources().getString(R.string.ss_stopwatch),
                getResources().getString(R.string.ts_app_shortcuts),
                getResources().getString(R.string.ss_exercise_opt),
                getResources().getString(R.string.ss_emergency_mode),
                getResources().getString(
                        R.string.ss_geo_news_m_application_name_abb) };
    }

    private void setActionBarColor(boolean isTransparent, float alpha) {

        Log.d(TAG, "setActionBarColor");
        ColorDrawable colorDrawable = new ColorDrawable();
        if (isTransparent) {
            colorDrawable
                    .setColor(getResources().getColor(R.color.transparent));
        } else {
            switch (mCurrentFragment) {

            case FRAGMENT_POSITION_MAIN:
                colorDrawable.setColor(getResources().getColor(
                        R.color.main_actionbar));

                break;

            case FRAGMENT_POSITION_BAROMETER:
                colorDrawable.setColor(getResources().getColor(
                        R.color.barometer_main_background));
                break;

            case FRAGMENT_POSITION_COMPASS:
                if (CompassConfig.KEY_PREF_VIEW_MODE == CompassConstants.VIEW_MODE_NORMAL) {
                    colorDrawable.setColor(getResources().getColor(
                            R.color.compass_background_normal));
                } else if (CompassConfig.KEY_PREF_VIEW_MODE == CompassConstants.VIEW_MODE_CAMERA
                        || CompassConfig.KEY_PREF_VIEW_MODE == CompassConstants.VIEW_MODE_MAP) {
                    colorDrawable.setColor(getResources().getColor(
                            R.color.compass_location_layout_mini));
                }
                break;
            case FRAGMENT_POSITION_STOPWATCH:
                colorDrawable.setColor(getResources().getColor(
                        R.color.stopwatch_bg));
                break;
            case FRAGMENT_POSITION_TORCH:
                colorDrawable.setColor(getResources().getColor(
                        R.color.flash_bg_main_original));
                break;
            default:
                break;
            }
            colorDrawable.setAlpha((int) (alpha * 255));
        }
        getActionBar().setBackgroundDrawable(colorDrawable);
    }

    private OnPageChangeListener mOnPagerChange = new OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            if (Lib.DEBUGGABLE)
                Log.d(TAG, "changeIndicator() position = " + position);
            changeIndicator(position);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset,
                int positionOffsetPixels) {

            mIsScrolling = isScrolling(positionOffset);

            if (mListFragment.size() > 2) {
                updateActionBar(position, positionOffset);
                updateCompassCameraView(position, positionOffset);
            }
        }

        @Override
        public void onPageScrollStateChanged(int position) {
            mIsScrolling = false;
        }
    };

    private boolean isScrolling(float positionOffset) {
        if ((positionOffset % 1) != 0.0f) {
            return true;
        }
        return false;
    }

    private void updateCompassCameraView(int position, float positionOffset) {
        float leftOfPage = position + positionOffset;

        if (leftOfPage > 0.0f
                && leftOfPage < 2.0f
                && CompassConfig.KEY_PREF_VIEW_MODE == CompassConstants.VIEW_MODE_CAMERA) {
            mSurfaceView.setVisibility(View.VISIBLE);
        } else {
            mSurfaceView.setVisibility(View.GONE);
        }
    }

    private void updateActionBar(int position, float positionOffset) {

        float leftOfPage = position + positionOffset;

        if (leftOfPage <= 0.5f) {
            getActionBar().setTitle(R.string.ss_barometer);
            mBarometerFragment.setMenuVisibility(true);
            mCompassFragment.setMenuVisibility(false);
            mStopwatchFragment.setMenuVisibility(false);
            mTorchFragment.setMenuVisibility(false);
        } else if (leftOfPage > 0.5f && leftOfPage <= 1.5f) {
            getActionBar().setTitle(R.string.ss_compass);
            mTorchFragment.setMenuVisibility(false);
            mCompassFragment.setMenuVisibility(true);
            mBarometerFragment.setMenuVisibility(false);
            mStopwatchFragment.setMenuVisibility(false);
        } else if (leftOfPage > 1.5f && leftOfPage <= 2.5f) {
            getActionBar().setTitle(R.string.ss_torch_light_abb);
            mStopwatchFragment.setMenuVisibility(false);
            mCompassFragment.setMenuVisibility(false);
            mTorchFragment.setMenuVisibility(true);
            mBarometerFragment.setMenuVisibility(false);
        } else {
            getActionBar().setTitle(R.string.ss_stopwatch);
            mBarometerFragment.setMenuVisibility(false);
            mCompassFragment.setMenuVisibility(false);
            mTorchFragment.setMenuVisibility(false);
            mStopwatchFragment.setMenuVisibility(true);
        }
    }

    public void changeIndicator(int position) {

        mIsScrolling = false;

        if (mListFragment.size() < 2) {
            return;
        }

        int preFragment = mCurrentFragment;
        mCurrentFragment = position;
        if (Lib.DEBUGGABLE)
            Log.d(TAG, "changeIndicator " + preFragment + " -> " + position);
        mSelectedAppsAdapter.notifyDataSetChanged();

        // stop speak lap time
        if (preFragment == FRAGMENT_POSITION_STOPWATCH) {
            mStopwatchFragment.stopSpeak();
        }

        switch (position) {

        case FRAGMENT_POSITION_BAROMETER:

            changeScreenMode(getScreenMode(mPrefs,
                    BarometerSettingsActivity.KEY_PREF_BARO_SCREEN_MODE));

            mBarometerFragment.onStartFragment(this);

            if (preFragment == FRAGMENT_POSITION_TORCH) {
                if (mTorchFragment.getCurrentTorchMode() == TorchFragment.MORSE_CODE_MODE) {
                    mTorchFragment.hideKeyboard();
                } else if (mTorchFragment.getCurrentTorchMode() == TorchFragment.SCREEN_LIGHT_MODE) {
                    getActionBar().show();
                    mTorchFragment.resetFlagEmtyScreen(false);
                    TorchFragment.mScreenControlLayout
                            .setVisibility(View.VISIBLE);
                    TorchFragment.mIsScreenUsing = false;
                    mTorchFragment.resetSystemBrightness();
                }
            }
            mImgIndicatorBaro
                    .setImageResource(R.drawable.active_mode_inadicator_pre);
            mImgIndicatorCompass
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorFlash
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorStopwatch
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            break;

        case FRAGMENT_POSITION_COMPASS:

            mBarometerFragment.saveLastTime(this);

            changeScreenMode(getScreenMode(mPrefs,
                    CompassPreferenceActivity.KEY_PREF_COMPASS_SCREEN_MODE));

            mCompassFragment.onStartFragment(this);

            if (preFragment == FRAGMENT_POSITION_TORCH) {
                if (mTorchFragment.getCurrentTorchMode() == TorchFragment.MORSE_CODE_MODE) {
                    mTorchFragment.hideKeyboard();
                } else if (mTorchFragment.getCurrentTorchMode() == TorchFragment.SCREEN_LIGHT_MODE) {
                    getActionBar().show();
                    mTorchFragment.resetFlagEmtyScreen(false);
                    TorchFragment.mScreenControlLayout
                            .setVisibility(View.VISIBLE);
                    TorchFragment.mIsScreenUsing = false;
                    mTorchFragment.resetSystemBrightness();
                }
            }
            mImgIndicatorBaro
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorCompass
                    .setImageResource(R.drawable.active_mode_inadicator_pre);
            mImgIndicatorFlash
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorStopwatch
                    .setImageResource(R.drawable.active_mode_inadicator_nor);

            break;

        case FRAGMENT_POSITION_TORCH:

            if (mTorchFragment.getCurrentTorchMode() == TorchFragment.SCREEN_LIGHT_MODE) {
                changeScreenMode(getScreenMode(mPrefs,
                        FlashSettingsActivity.KEY_PREF_SCREEN_LIGHT_SCREEN_MODE));
                TorchFragment.mIsScreenUsing = true;
                if (mTorchFragment.isStopScreenColorThread()) {
                    mTorchFragment.executeScreenColorChange();
                }
                mTorchFragment.updateSystemBrightness(false);
            } else {

                changeScreenMode(getScreenMode(mPrefs,
                        FlashSettingsActivity.KEY_PREF_FLASH_SCREEN_MODE));

                if (mTorchFragment.getCurrentTorchMode() == TorchFragment.TORCH_LIGHT_MODE) {
                    mTorchFragment.showFlashWarning();
                } else if (mTorchFragment.getCurrentTorchMode() == TorchFragment.MORSE_CODE_MODE) {
                    if (TorchFragment.mMorseMessage != null) {
                        TorchFragment.mMorseMessage.requestFocus();
                    }
                    mTorchFragment.showMorseWarning();
                }
            }

            mImgIndicatorBaro
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorCompass
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorFlash
                    .setImageResource(R.drawable.active_mode_inadicator_pre);
            mImgIndicatorStopwatch
                    .setImageResource(R.drawable.active_mode_inadicator_nor);

            break;
        case FRAGMENT_POSITION_STOPWATCH:

            changeScreenMode(getScreenMode(mPrefs,
                    getResources()
                            .getString(R.string.stopwatch_screen_mode_key)));

            if (preFragment == FRAGMENT_POSITION_TORCH) {
                if (mTorchFragment.getCurrentTorchMode() == TorchFragment.MORSE_CODE_MODE) {
                    mTorchFragment.hideKeyboard();
                } else if (mTorchFragment.getCurrentTorchMode() == TorchFragment.SCREEN_LIGHT_MODE) {
                    getActionBar().show();
                    mTorchFragment.resetFlagEmtyScreen(false);
                    TorchFragment.mScreenControlLayout
                            .setVisibility(View.VISIBLE);
                    TorchFragment.mIsScreenUsing = false;
                    mTorchFragment.resetSystemBrightness();
                }
            }
            mImgIndicatorBaro
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorCompass
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorFlash
                    .setImageResource(R.drawable.active_mode_inadicator_nor);
            mImgIndicatorStopwatch
                    .setImageResource(R.drawable.active_mode_inadicator_pre);
            break;

        default:
            break;
        }
    }

    private void initActionBar() {

        Log.d(TAG, "initActionBar");
        mDrawerMenuContentDescription = getResources().getString(
                R.string.ss_drawer_menu_tts)
                + ", "
                + getResources()
                        .getString(R.string.ss_double_tap_to_open_t_tts);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFormat(PixelFormat.UNKNOWN);
        getActionBar().setDisplayShowTitleEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    public void createNotification() {

        Intent intent = new Intent("com.sec.activemode.flashlight.offFlash");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                intent, 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext());
        mBuilder.setSmallIcon(R.drawable.main_icon_torch);
        mBuilder.setContentTitle(getString(R.string.ts_torch_light_button_abb));
        mBuilder.setContentText(getString(R.string.ss_tap_here_to_turn_off_torch_sbody));
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setTicker(getString(R.string.ss_tap_here_to_turn_off_torch_sbody));
        mBuilder.setAutoCancel(true);
        mBuilder.setOngoing(true);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public void cancelNotification() {

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    private void loadDefaultAppsFromXML() {

        Log.d(TAG, "loadDefaultAppsFromXML");
        mActivityMateProvider = new ActivityMateProvider(this);
        mDefaultApps = mActivityMateProvider.getDefaultApps();
    }

    public static void loadSelectedAppsFromDB() {
        if (Lib.DEBUGGABLE)
            Log.d(TAG, "loadSelectedAppsFromDB "
                    + ManageDrawerActivity.mIsInManageDrawer);
        if (ManageDrawerActivity.mIsInManageDrawer) {
            return;
        }
        mSelectedApps = mActivityMateProvider
                .getUserSelectedApps(ActivityMateProvider.TABLE_SELECTED_APPS);
        if (Lib.DEBUGGABLE)
            Log.d(TAG, "loadDefaultAppsFromDB " + mSelectedApps.size());

        if (mSelectedAppsAdapter != null) {
            mSelectedAppsAdapter.setSelectedApps(mSelectedApps);
            mSelectedAppsAdapter.notifyDataSetChanged();
        }
    }

    public static boolean isDefaultApps(AppInfo appInfo) {

        for (AppInfo checkApp : mDefaultApps) {
            if (appInfo.getClassName().equals(checkApp.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {

        Log.d(TAG, "onClick()");
        switch (v.getId()) {
        case R.id.activitymate_reorder_button:
            Intent i = new Intent(this, ManageDrawerActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(i, REQUEST_REORDER);
            break;
        default:
            break;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {

        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        Log.d(TAG, "onConfigurationChanged, mIsScrolling = " + mIsScrolling);

        super.onConfigurationChanged(newConfig);

        if (mIsScrolling) {
            mSectionsPagerAdapter.notifyDataSetChanged();
        }
        mDrawerToggle.onConfigurationChanged(newConfig);

        if (mCurrentFragment == FRAGMENT_POSITION_COMPASS
                && CompassConfig.KEY_PREF_VIEW_MODE == CompassConstants.VIEW_MODE_CAMERA) {
            mSurfaceView.setVisibility(View.VISIBLE);
        } else {
            mSurfaceView.setVisibility(View.GONE);
        }

        setDrawerMarginTop((int) getResources().getDimension(
                R.dimen.stopwatch_action_bar_height));

    }

    private void setDrawerMarginTop(int value) {

        LinearLayout drawerLayout = (LinearLayout) findViewById(R.id.activitymate_drawer);

        android.support.v4.widget.DrawerLayout.LayoutParams mParamsLV = (android.support.v4.widget.DrawerLayout.LayoutParams) drawerLayout
                .getLayoutParams();

        mParamsLV.topMargin = value;

        drawerLayout.setLayoutParams(mParamsLV);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view,
                int position, long id) {

            AppInfo appInfo = (AppInfo) view.getTag();

            if (appInfo != null) {
                if (ActivityMateProvider.PACKAGE_NAME.equals(appInfo
                        .getPackageName())) {
                    if (mContext.getResources()
                            .getString(R.string.ss_main_screen)
                            .equals(appInfo.getAppName())) {
                        switchtoMainFragment(true);
                    } else if (mContext.getResources()
                            .getString(R.string.ss_exercise_opt)
                            .equals(appInfo.getAppName())) {
                        startExercisePro();
                    } else if (mContext.getResources()
                            .getString(R.string.ss_barometer)
                            .equals(appInfo.getAppName())) {
                        switchFragment(FRAGMENT_POSITION_BAROMETER, true);
                    } else if (mContext.getResources()
                            .getString(R.string.ss_compass)
                            .equals(appInfo.getAppName())) {
                        switchFragment(FRAGMENT_POSITION_COMPASS, true);
                    } else if (mContext.getResources()
                            .getString(R.string.ss_stopwatch)
                            .equals(appInfo.getAppName())) {
                        switchFragment(FRAGMENT_POSITION_STOPWATCH, true);
                    } else if (mContext.getResources()
                            .getString(R.string.ss_torch_light_abb)
                            .equals(appInfo.getAppName())) {
                        switchFragment(FRAGMENT_POSITION_TORCH, true);
                    } else if (mContext.getResources()
                            .getString(R.string.ss_emergency_mode)
                            .equals(appInfo.getAppName())) {
                        startEmergenceMode();
                    } else if (mContext
                            .getResources()
                            .getString(
                                    R.string.ss_geo_news_m_application_name_abb)
                            .equals(appInfo.getAppName())) {
                        startGeoNews();
                    } else {
                        mShowShortcuts = !mShowShortcuts;
                        mSelectedAppsAdapter.notifyDataSetChanged();
                    }
                } else {
                    Intent intent = new Intent();
                    intent.setComponent(appInfo.getComponentName());

                    if (appInfo.getPackageName().equals(getPackageName())) {
                        intent.setAction(Intent.ACTION_MAIN);
                        intent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                        startActivity(intent);
                    } else {
                        Lib.launchApp(intent, getBaseContext());
                    }
                }
            } else {
                showAppsList();
            }
        }
    };

    private void showAppsList() {
        Log.d(TAG, "Show apps list ");

        Intent i = new Intent();
        i.setClass(this, MenuAppsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(i, REQUEST_ADD);
    }

    protected void startExercisePro() {
        if (isTheFirstLaunchSHealth) {
            Intent intent = new Intent(Lib.SHEALTH_CLASS_CHECK_INIT);
            startActivityForResult(intent, REQUEST_CODE_SHEALTH);
            isTheFirstLaunchSHealth = false;
        }
    }

    public void startEmergenceMode() {
        Intent intent = new Intent(
                EmergencyConstants.EMERGENCY_START_SERVICE_BY_ORDER);
        intent.putExtra("enabled", true); // or intent.putExtra("enabled",
                                          // false);
        intent.putExtra("flag", EmergencyConstants.MANUAL);
        sendBroadcast(intent);
    }

    public void startGeoNews() {
        Intent intent = new Intent("com.sec.android.GeoLookout.geolifeactivity");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public static void removeASelectedApp(int pos, boolean isRemoveMulti) {

        AppInfo app = mSelectedApps.get(pos);
        if (Lib.DEBUGGABLE)
            Log.d(TAG, "removeASelectedApps " + app);
        mSelectedApps.remove(app);

        mActivityMateProvider.deleteSelectedApp(mActivityMateProvider.database,
                ActivityMateProvider.TABLE_SELECTED_APPS, "className = ?",
                new String[] { app.getClassName().toString() });
        if (!isRemoveMulti) {
            mSelectedAppsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Lib.DEBUGGABLE)
            Log.d(TAG, "onActivityResult " + requestCode + " " + resultCode);

        switch (requestCode) {
        case REQUEST_CODE_TORCH:
            boolean isUpdateDefault;

            if(data == null){
                isUpdateDefault = false;
            }else{
                isUpdateDefault = data.getBooleanExtra("updated_default",
                    false);
            }
            if (isUpdateDefault) {
                mTorchFragment
                        .switchMode(mTorchFragment.getDefautlMode(), true);
            }

            break;

        case REQUEST_REORDER:

            loadSelectedAppsFromDB();

            break;

        case REQUEST_CODE_SHEALTH:

            if (resultCode == Activity.RESULT_FIRST_USER) { // information is
                // already inited
                ComponentName componentName = new ComponentName(
                        Lib.SHEALTH_PACKAGE_NAME,
                        Lib.SHEALTH_CLASS_NAME_EXERCISE);
                Intent intent = new Intent();
                intent.setComponent(componentName);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else if (resultCode == Activity.RESULT_OK) { // need init S-health
                // inform
                ComponentName componentName = new ComponentName(
                        Lib.SHEALTH_PACKAGE_NAME, Lib.SHEALTH_CLASS_NAME_ROOT);
                Intent intent = new Intent();
                intent.setComponent(componentName);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

            break;

        case REQUEST_ADD:

            if (resultCode == RESULT_OK) {
                loadSelectedAppsFromDB();
            }

            break;

        case REQUEST_TERMS_CONDITIONS:
            if (resultCode != RESULT_OK) {
                finish();
            }
            break;

        default:
            break;
        }

    }

    public static void updateSelectedApps() {
        if (Lib.DEBUGGABLE)
            Log.d(TAG, "updateSelectedApps " + mSelectedApps.size());
        int pos = 0;
        for (AppInfo app : mSelectedApps) {
            mActivityMateProvider.updateAppInfo(app, pos);
            pos++;
        }
    }

    private void initFragment() {

        if (mBarometerFragment == null) {
            mBarometerFragment = new BarometerFragment();
        }
        if (mCompassFragment == null) {
            mCompassFragment = new CompassFragment();
        }
        if (mStopwatchFragment == null) {
            mStopwatchFragment = new Stopwatch();
        }
        if (mTorchFragment == null) {
            mTorchFragment = new TorchFragment();
        }
    }

    private void closeDrawer(int milisecond) {
        if (mMainLayout != null & mMainLayout.isDrawerOpen(GravityCompat.START)) {
            mMainLayout.postDelayed(new Runnable() {

                @Override
                public void run() {
                    mMainLayout.closeDrawers();
                }
            }, milisecond);

        }
    }

    public void switchFragment(int fragmentPosition, boolean fromDrawer) {
        if (Lib.DEBUGGABLE)
            Log.d(TAG, "switchFragment to position:" + fragmentPosition);

        if (fromDrawer && mCurrentFragment == fragmentPosition) {
            closeDrawer(0);
            return;
        }
        getActionBar().setTitle(
                getString(R.string.ss_activity_zone_m_application_name));

        if (mCurrentFragment == FRAGMENT_POSITION_COMPASS
                && CompassConfig.KEY_PREF_VIEW_MODE == CompassConstants.VIEW_MODE_CAMERA) {
            mSurfaceView.setVisibility(View.VISIBLE);
        }

        initFragment();

        if (mListFragment.size() <= 1) {
            mListFragment.clear();

            mListFragment.add(mBarometerFragment);
            mListFragment.add(mCompassFragment);
            mListFragment.add(mTorchFragment);
            mListFragment.add(mStopwatchFragment);

            if (mSectionsPagerAdapter == null) {
                mSectionsPagerAdapter = new SectionsPagerAdapter(
                        getFragmentManager(), mListFragment);
                mViewPager.setAdapter(mSectionsPagerAdapter);
            }

            mSectionsPagerAdapter.notifyDataSetChanged();

            if (fragmentPosition == FRAGMENT_POSITION_BAROMETER) {
                changeIndicator(FRAGMENT_POSITION_BAROMETER);
            }
        }

        mTorchFragment.getSystemBrightness();
        mTorchFragment.getSystemBrightnessMode();

        mViewPager.setCurrentItem(fragmentPosition, false);

        mSelectedAppsAdapter.notifyDataSetChanged();

        closeDrawer(Lib.TIME_TO_CLOSE);
        if (fromDrawer) {
            setActionBarColor(false, 1);
        }
        mIndicatorLayout.setVisibility(View.VISIBLE);

    }

    public void switchtoMainFragment(boolean fromDrawer) {

        Log.d(TAG, "switchFragment to Main");

        if (fromDrawer && mCurrentFragment == FRAGMENT_POSITION_MAIN) {
            closeDrawer(0);
            return;
        }

        mIsScrolling = false;

        getActionBar().show();

        CompassConfig.KEY_PREF_VIEW_MODE_OLD = -1;
        mSurfaceView.setVisibility(View.GONE);

        if (mListFragment.size() > 1 || mListFragment.size() == 0) {
            mListFragment.clear();

            if (mMainFragment == null) {
                mMainFragment = new MainFragment();
            }
            mListFragment.add(mMainFragment);
            mCurrentFragment = FRAGMENT_POSITION_MAIN;
            mSelectedAppsAdapter.notifyDataSetChanged();

            if (mSectionsPagerAdapter == null) {

                mSectionsPagerAdapter = new SectionsPagerAdapter(
                        getFragmentManager(), mListFragment);
                mViewPager.setAdapter(mSectionsPagerAdapter);
            }
            mViewPager.removeAllViews();
            mSectionsPagerAdapter.notifyDataSetChanged();
            mIndicatorLayout.setVisibility(View.GONE);
        }
        closeDrawer(Lib.TIME_TO_CLOSE);
        if (fromDrawer) {
            setActionBarColor(false, 1);
        }
        changeScreenMode(Lib.TURN_OFF_SCREEN);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        Log.d(TAG, "onKeyUp()");
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {

        Log.d(TAG, "onKeyDown()");

        switch (keyCode) {
        case KeyEvent.KEYCODE_BACK:
            if (mListFragment.size() > 1) {
                switchtoMainFragment(false);
                return true;
            }

        default:
            if (mCurrentFragment == FRAGMENT_POSITION_STOPWATCH
                    && mStopwatchFragment.isUsingHardKey()
                    && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN || keyEvent
                            .getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP)) {
                return true;
            } else {
                break;
            }
        }

        return super.onKeyDown(keyCode, keyEvent);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if (mCurrentFragment == FRAGMENT_POSITION_STOPWATCH
                && mStopwatchFragment.isUsingHardKey()
                && event.getAction() == KeyEvent.ACTION_UP
                && (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN || event
                        .getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP)) {
            return mStopwatchFragment.dispatchKeyEvent(event);
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart()");
        super.onRestart();
        updateCurrentScreenMode();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();

        if (mCurrentFragment == FRAGMENT_POSITION_COMPASS
                && CompassConfig.KEY_PREF_VIEW_MODE == CompassConstants.VIEW_MODE_CAMERA) {
            mSurfaceView.setVisibility(View.VISIBLE);
        }

        new Handler().postDelayed(new Runnable() {
            public void run() {

                isTheFirstLaunchSHealth = true;

            }
        }, 300);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");

        super.onSaveInstanceState(outState);

        outState.putInt(CURRENTFRAGMENT_KEY, mCurrentFragment);

        outState.putBoolean(DRAWERISOPEN_KEY, mIsOpeningDrawer);

        if (mCurrentFragment != FRAGMENT_POSITION_MAIN) {
            saveFragments(outState);
        }

    }

    private void saveFragments(Bundle bundle) {
        try { // try for Fragment is not currently in the FragmentManager
              // issue :P140619-02536
            switch (mCurrentFragment) {
            case FRAGMENT_POSITION_BAROMETER:
                if (mBarometerFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, BAROMETER_KEY,
                            mBarometerFragment);
                }
                if (mCompassFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, COMPASS_KEY,
                            mCompassFragment);
                }
                break;
            case FRAGMENT_POSITION_COMPASS:
                if (mBarometerFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, BAROMETER_KEY,
                            mBarometerFragment);
                }
                if (mCompassFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, COMPASS_KEY,
                            mCompassFragment);
                }
                if (mTorchFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, FLASHLIGHT_KEY,
                            mTorchFragment);
                }
                break;
            case FRAGMENT_POSITION_TORCH:
                if (mCompassFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, COMPASS_KEY,
                            mCompassFragment);
                }
                if (mTorchFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, FLASHLIGHT_KEY,
                            mTorchFragment);
                }
                if (mStopwatchFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, STOPWATCH_KEY,
                            mStopwatchFragment);
                }
                break;
            case FRAGMENT_POSITION_STOPWATCH:
                if (mTorchFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, FLASHLIGHT_KEY,
                            mTorchFragment);
                }
                if (mStopwatchFragment.isAdded()) {
                    getFragmentManager().putFragment(bundle, STOPWATCH_KEY,
                            mStopwatchFragment);
                }
                break;
            default:
                break;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void restoreFragments(Bundle bundle) {
        switch (mCurrentFragment) {
        case FRAGMENT_POSITION_BAROMETER:
            mBarometerFragment = (BarometerFragment) getFragmentManager()
                    .getFragment(bundle, BAROMETER_KEY);
            if (mBarometerFragment == null) {
                mBarometerFragment = new BarometerFragment();
            }
            mCompassFragment = (CompassFragment) getFragmentManager()
                    .getFragment(bundle, COMPASS_KEY);
            if (mCompassFragment == null) {
                mCompassFragment = new CompassFragment();
            }
            break;
        case FRAGMENT_POSITION_COMPASS:
            mBarometerFragment = (BarometerFragment) getFragmentManager()
                    .getFragment(bundle, BAROMETER_KEY);
            if (mBarometerFragment == null) {
                mBarometerFragment = new BarometerFragment();
            }
            mCompassFragment = (CompassFragment) getFragmentManager()
                    .getFragment(bundle, COMPASS_KEY);
            if (mCompassFragment == null) {
                mCompassFragment = new CompassFragment();
            }
            mTorchFragment = (TorchFragment) getFragmentManager().getFragment(
                    bundle, FLASHLIGHT_KEY);
            if (mTorchFragment == null) {
                mTorchFragment = new TorchFragment();
            }
            break;
        case FRAGMENT_POSITION_TORCH:
            mCompassFragment = (CompassFragment) getFragmentManager()
                    .getFragment(bundle, COMPASS_KEY);
            if (mCompassFragment == null) {
                mCompassFragment = new CompassFragment();
            }
            mTorchFragment = (TorchFragment) getFragmentManager().getFragment(
                    bundle, FLASHLIGHT_KEY);
            if (mTorchFragment == null) {
                mTorchFragment = new TorchFragment();
            }
            mStopwatchFragment = (Stopwatch) getFragmentManager().getFragment(
                    bundle, STOPWATCH_KEY);
            if (mStopwatchFragment == null) {
                mStopwatchFragment = new Stopwatch();
            }
            break;
        case FRAGMENT_POSITION_STOPWATCH:
            mTorchFragment = (TorchFragment) getFragmentManager().getFragment(
                    bundle, FLASHLIGHT_KEY);
            if (mTorchFragment == null) {
                mTorchFragment = new TorchFragment();
            }
            mStopwatchFragment = (Stopwatch) getFragmentManager().getFragment(
                    bundle, STOPWATCH_KEY);
            if (mStopwatchFragment == null) {
                mStopwatchFragment = new Stopwatch();
            }
            break;
        default:
            break;
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);
        Log.d(TAG, "onRestoreInstanceState");
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause()");
        super.onPause();
        mSurfaceView.setVisibility(View.GONE);
        releaseCamera();
        System.gc();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop()");
        super.onStop();
        PowerControllerScreen.release();
        boolean isFlashOn = false;
        if (mTorchFragment != null) {
            if (mTorchFragment.getCurrentTorchMode() == TorchFragment.TORCH_LIGHT_MODE) {
                isFlashOn = TorchFragment.mIsTorchOn;
            } else if (mTorchFragment.getCurrentTorchMode() == TorchFragment.MORSE_CODE_MODE) {
                isFlashOn = TorchFragment.mIsMorseRunning;
            }
        }

        if (!isFlashOn) {
            return;
        }

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (pm.isScreenOn()) {
            createNotification();
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");
        super.onDestroy();
        mViewPager.removeAllViews();
        getApplicationContext().unregisterReceiver(mReceiverUninstall);
    }

    private BroadcastReceiver mReceiverUninstall = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String details = intent.getDataString();
            String[] packagename = null;
            if (details != null)
                packagename = details.split(":");
            if (Lib.DEBUGGABLE)
                Log.d(TAG, "received Uninstall intent " + intent.getAction()
                        + " " + packagename);
            if (packagename != null && packagename.length > 1
                    && packagename[1] != null) {

                if (intent.getAction() == Intent.ACTION_PACKAGE_REMOVED
                        || intent.getAction() == Intent.ACTION_PACKAGE_CHANGED) {
                    if (mSelectedApps != null) {
                        for (int i = 0; i < mSelectedApps.size(); i++) {
                            if (mSelectedApps.get(i).getPackageName()
                                    .equals(packagename[1])) {
                                removeASelectedApp(i, false);
                                break;
                            }
                        }
                    }
                }
            }
        }
    };

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
        if (Lib.DEBUGGABLE)
            Log.d(TAG, "surfaceChanged(), holder = " + holder + " camera = "
                    + mCamera);
        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void createCamera() {
        if (mCamera == null) {
            try {
                mCamera = Camera.open();

                int width = getResources().getDisplayMetrics().widthPixels;
                int height = getResources().getDisplayMetrics().heightPixels;
                if (width < height) {
                    Log.i(TAG,
                            "Display reports portrait orientation; assuming this is incorrect");
                    int temp = width;
                    width = height;
                    height = temp;
                }

                mScreenResolution = new Point(width, height);

                mParameters = mCamera.getParameters();
                mParameters.setPreviewSize(mScreenResolution.x,
                        mScreenResolution.y);
                mParameters
                        .setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                mCamera.setParameters(mParameters);
                mCamera.setDisplayOrientation(0x00010000);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(final SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated(), holder = " + holder);

        createCamera();

        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(holder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mCamera.startPreview();
        } else {
            mCompassFragment.showCameraError();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "surfaceDestroyed()");

        if (mCamera != null) {
            mCamera.stopPreview();
        }
    }

    private void releaseCamera() {
        Log.d(TAG, "releaseCamera()");

        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    public static String getSaleCode() {
        String saleCode = "";
        try {
            saleCode = SystemProperties.get("ro.csc.sales_code");
            if (TextUtils.isEmpty(saleCode)) {
                saleCode = SystemProperties.get("ril.sales_code");
            }
        } catch (Exception e) {
            Log.d("getSaleCode", "readSalesCode failed");
        }
        Log.d(TAG, "getSaleCode " + saleCode);
        return saleCode;
    }

    public int getScreenMode(SharedPreferences sharePrefer, String keyString) {
        return (Integer.parseInt(sharePrefer.getString(keyString,
                String.valueOf(Lib.DIM_SCREEN))));
    }

    /*
     * @Override public void onSharedPreferenceChanged(SharedPreferences
     * sharePrefer, String key) { if
     * (key.equals(BarometerSettingsActivity.KEY_PREF_BARO_SCREEN_MODE) ||
     * key.equals(CompassPreferenceActivity.KEY_PREF_COMPASS_SCREEN_MODE) ||
     * key.equals(FlashSettingsActivity.KEY_PREF_FLASH_SCREEN_MODE) ||
     * key.equals(FlashSettingsActivity.KEY_PREF_SCREEN_LIGHT_SCREEN_MODE) ||
     * key.equals(getResources().getString(
     * R.string.stopwatch_screen_mode_key))) {
     * changeScreenMode(getScreenMode(sharePrefer, key)); } }
     */

    // Method for trace code
    /*
     * public static String getCurrentMethodName() { return
     * Thread.currentThread().getStackTrace()[2].getClassName() + "." +
     * Thread.currentThread().getStackTrace()[2].getMethodName(); }
     */

    public void changeScreenMode(int mode) {

        Log.d(TAG, "changeScreenMode :" + mode);
        switch (mode) {
        case Lib.SCREEN_ALWAYS_ON:
            PowerControllerScreen.release();
            getWindow()
                    .addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            break;
        case Lib.TURN_OFF_SCREEN:
            PowerControllerScreen.release();
            getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            break;
        case Lib.DIM_SCREEN:
            getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            PowerControllerScreen.acquire(this);
            break;

        default:
            break;
        }
    }

    private void updateCurrentScreenMode() {
        Log.d(TAG, "updateCurrentScreenMode");

        switch (mCurrentFragment) {
        case FRAGMENT_POSITION_BAROMETER:
            changeScreenMode(getScreenMode(mPrefs,
                    BarometerSettingsActivity.KEY_PREF_BARO_SCREEN_MODE));
            break;
        case FRAGMENT_POSITION_COMPASS:
            changeScreenMode(getScreenMode(mPrefs,
                    CompassPreferenceActivity.KEY_PREF_COMPASS_SCREEN_MODE));
            break;
        case FRAGMENT_POSITION_TORCH:
            if (mTorchFragment.getCurrentTorchMode() == TorchFragment.SCREEN_LIGHT_MODE) {
                changeScreenMode(getScreenMode(mPrefs,
                        FlashSettingsActivity.KEY_PREF_SCREEN_LIGHT_SCREEN_MODE));
            } else {
                changeScreenMode(getScreenMode(mPrefs,
                        FlashSettingsActivity.KEY_PREF_FLASH_SCREEN_MODE));
            }
            break;
        case FRAGMENT_POSITION_STOPWATCH:
            changeScreenMode(getScreenMode(mPrefs,
                    getResources()
                            .getString(R.string.stopwatch_screen_mode_key)));
            break;

        default:
            changeScreenMode(Lib.TURN_OFF_SCREEN);
            break;
        }
    }

    public static boolean isTouchExplorationEnabled() {
        return (mAccessibilityManager != null) && mAccessibilityManager.isTouchExplorationEnabled();
    }
}
