package com.jostruongngoc.ailatrieuphudemo.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import com.jostruongngoc.ailatrieuphudemo.R;

public class MainActivity extends Activity implements View.OnClickListener{
    // add
//    private static final int REQUEST_CONNECT_DEVICE = 1;

    private Button btnTrial;
    private Button btnMatchBlueTooth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }
    private void initViews() {
        btnTrial = (Button) findViewById(R.id.btn_play_trial);
        btnTrial.setOnClickListener(this);

        btnMatchBlueTooth = (Button) findViewById(R.id.btnMatchUseBlueTooth);
        btnMatchBlueTooth.setOnClickListener(this);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_button_trial);
        btnTrial.startAnimation(animation);
        btnMatchBlueTooth.startAnimation(animation);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_play_trial:
                Intent intent = new Intent(MainActivity.this, PlayGameActivity.class);
                startActivity(intent);
                break;

            case R.id.btnMatchUseBlueTooth:
                Intent i = new Intent(this, BlueToothChat.class);
                startActivity(i);
                break;
        }
    }
}
