package com.jostruongngoc.ailatrieuphudemo.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.jostruongngoc.ailatrieuphudemo.R;

/**
 * Created by Jos.TruongNgoc on 21-07-2016.
 */
public class SplashScreenActivity extends Activity {
    private ImageView ivLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ivLogo = (ImageView) findViewById(R.id.iv_logo_company);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_splash_screen);
        ivLogo.startAnimation(animation);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }
}
