package com.jostruongngoc.ailatrieuphudemo.handing_data;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jos.TruongNgoc on 18-07-2016.
 */
public class DBManager {
    private static final String PATH_DATA = Environment.getDataDirectory().getPath() +
            "/data/com.jostruongngoc.ailatrieuphudemo/database";
    private static final String DATA_NAME = "Question";
    private static final String GET_15_QUESTIONS = "SELECT * FROM (SELECT * FROM Question ORDER BY RANDOM()) " +
            "GROUP BY level ORDER BY level LIMIT 15";

    private SQLiteDatabase mSqLiteDatabase;
    private List<Question> arrQs;

    public DBManager(Context context) {
        arrQs = new ArrayList<>();
        copyData(context);
    }

    public Question getOneQues(int level) {
        openDB();
        Cursor cursor = mSqLiteDatabase.rawQuery("select * from Question where level = '" + level +
                "' order by random() limit 1", null);
        Question changeQues = new Question();
        if (cursor != null) {
            cursor.moveToFirst();
            int questionIndex = cursor.getColumnIndex("question");
            int levelIndex = cursor.getColumnIndex("level");
            int caseAIndex = cursor.getColumnIndex("casea");
            int caseBIndex = cursor.getColumnIndex("caseb");
            int caseCIndex = cursor.getColumnIndex("casec");
            int caseDIndex = cursor.getColumnIndex("cased");
            int trueCaseIndex = cursor.getColumnIndex("truecase");
            while (cursor.isAfterLast() == false) {
                String question, caseA, caseB, caseC, caseD;
                int levelQ, trueCase;
                question = cursor.getString(questionIndex);
                caseA = cursor.getString(caseAIndex);
                caseB = cursor.getString(caseBIndex);
                caseC = cursor.getString(caseCIndex);
                caseD = cursor.getString(caseDIndex);
                levelQ = Integer.parseInt(cursor.getString(levelIndex));
                trueCase = Integer.parseInt(cursor.getString(trueCaseIndex));
                
                changeQues = new Question(question, caseA, caseB, caseC, caseD, levelQ, trueCase);
                cursor.moveToNext();
            }
        }
        return changeQues;
    }

    public void get15questions() {
        openDB();
        Cursor cursor = mSqLiteDatabase.rawQuery(GET_15_QUESTIONS, null);

        if (cursor != null) {
            cursor.moveToFirst();
            int questionIndex = cursor.getColumnIndex("question");
            int levelIndex = cursor.getColumnIndex("level");
            int caseAIndex = cursor.getColumnIndex("casea");
            int caseBIndex = cursor.getColumnIndex("caseb");
            int caseCIndex = cursor.getColumnIndex("casec");
            int caseDIndex = cursor.getColumnIndex("cased");
            int trueCaseIndex = cursor.getColumnIndex("truecase");
            while (cursor.isAfterLast() == false) {
                String question, caseA, caseB, caseC, caseD;
                int level, trueCase;
                question = cursor.getString(questionIndex);
                caseA = cursor.getString(caseAIndex);
                caseB = cursor.getString(caseBIndex);
                caseC = cursor.getString(caseCIndex);
                caseD = cursor.getString(caseDIndex);
                level = Integer.parseInt(cursor.getString(levelIndex));
                trueCase = Integer.parseInt(cursor.getString(trueCaseIndex));

                arrQs.add(new Question(question, caseA, caseB, caseC, caseD, level, trueCase));

                cursor.moveToNext();
            }
        }
    }

    private void openDB() {
        if (mSqLiteDatabase == null || !mSqLiteDatabase.isOpen()) {
            mSqLiteDatabase = SQLiteDatabase.openDatabase(PATH_DATA + "/" + DATA_NAME, null, SQLiteDatabase.OPEN_READWRITE);
        }
    }

    public void closeDB() {
        if (mSqLiteDatabase != null && mSqLiteDatabase.isOpen()) {
            mSqLiteDatabase.close();
        }
    }

    private void copyData(Context context) {
        new File(PATH_DATA).mkdir();

        File file = new File(PATH_DATA + "/" +DATA_NAME);
        if (file.exists()) {
            return;
        }

        AssetManager assetManager = context.getAssets();
        try {
            InputStream inputStream = assetManager.open(DATA_NAME);
            FileOutputStream outputStream = new FileOutputStream(file);

            byte buff[] = new byte[1024];
            int length = inputStream.read(buff);
            while (length > 0) {
                outputStream.write(buff, 0, length);
                length = inputStream.read(buff);
            }
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Question> getArrQs() {
        return arrQs;
    }
}
