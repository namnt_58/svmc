package com.jostruongngoc.ailatrieuphudemo.custom_view;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TableRow;
import com.jostruongngoc.ailatrieuphudemo.R;

/**
 * Created by Jos.TruongNgoc on 29-07-2016.
 */
public class MoneyDialog extends Dialog{
    private static final int QUES_1 = 1;
    private static final int QUES_2 = 2;
    private static final int QUES_3 = 3;
    private static final int QUES_4 = 4;
    private static final int QUES_5 = 5;
    private static final int QUES_6 = 6;
    private static final int QUES_7 = 7;
    private static final int QUES_8 = 8;
    private static final int QUES_9 = 9;
    private static final int QUES_10 =10;
    private static final int QUES_11 = 11;
    private static final int QUES_12 = 12;
    private static final int QUES_13 = 13;
    private static final int QUES_14 = 14;
    private static final int QUES_15 = 15;

    private Button btnBoQua;
    private TableRow tbrQues1, tbrQues2, tbrQues3, tbrQues4, tbrQues5, tbrQues6, tbrQues7, tbrQues8, tbrQues9,
            tbrQues10, tbrQues11, tbrQues12, tbrQues13, tbrQues14, tbrQues15;

    public MoneyDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getAttributes().windowAnimations = R.style.MoneyDialogAnim;
        setContentView(R.layout.item_money_dialog);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        setCanceledOnTouchOutside(false);
        setCancelable(false);
        initViews();
    }

    public void showQuestion(int level) {
        switch (level) {
            case QUES_1:
                tbrQues1.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_2:
                tbrQues2.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_3:
                tbrQues3.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_4:
                tbrQues4.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_5:
                tbrQues5.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_6:
                tbrQues6.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_7:
                tbrQues7.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_8:
                tbrQues8.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_9:
                tbrQues9.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_10:
                tbrQues10.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_11:
                tbrQues11.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_12:
                tbrQues12.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_13:
                tbrQues13.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_14:
                tbrQues14.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
            case QUES_15:
                tbrQues15.setBackgroundResource(R.drawable.activity_player_image_money_curent);
                break;
        }
    }

    public void removeQues() {
        tbrQues1.setBackground(null);
        tbrQues2.setBackground(null);
        tbrQues3.setBackground(null);
        tbrQues4.setBackground(null);
        tbrQues5.setBackground(null);
        tbrQues6.setBackground(null);
        tbrQues7.setBackground(null);
        tbrQues8.setBackground(null);
        tbrQues9.setBackground(null);
        tbrQues10.setBackground(null);
        tbrQues11.setBackground(null);
        tbrQues12.setBackground(null);
        tbrQues13.setBackground(null);
        tbrQues14.setBackground(null);
        tbrQues15.setBackground(null);
    }

    private void initViews() {
        btnBoQua = (Button) findViewById(R.id.btn_bo_qua);
        tbrQues1 = (TableRow) findViewById(R.id.tbr_ques_1);
        tbrQues2 = (TableRow) findViewById(R.id.tbr_ques_2);
        tbrQues3 = (TableRow) findViewById(R.id.tbr_ques_3);
        tbrQues4 = (TableRow) findViewById(R.id.tbr_ques_4);
        tbrQues5 = (TableRow) findViewById(R.id.tbr_ques_5);
        tbrQues6 = (TableRow) findViewById(R.id.tbr_ques_6);
        tbrQues7 = (TableRow) findViewById(R.id.tbr_ques_7);
        tbrQues8 = (TableRow) findViewById(R.id.tbr_ques_8);
        tbrQues9 = (TableRow) findViewById(R.id.tbr_ques_9);
        tbrQues10 = (TableRow) findViewById(R.id.tbr_ques_10);
        tbrQues11= (TableRow) findViewById(R.id.tbr_ques_11);
        tbrQues12 = (TableRow) findViewById(R.id.tbr_ques_12);
        tbrQues13 = (TableRow) findViewById(R.id.tbr_ques_13);
        tbrQues14 = (TableRow) findViewById(R.id.tbr_ques_14);
        tbrQues15 = (TableRow) findViewById(R.id.tbr_ques_15);
    }

    public TableRow getTbrQues1() {
        return tbrQues1;
    }

    public TableRow getTbrQues2() {
        return tbrQues2;
    }

    public TableRow getTbrQues3() {
        return tbrQues3;
    }

    public TableRow getTbrQues4() {
        return tbrQues4;
    }

    public TableRow getTbrQues5() {
        return tbrQues5;
    }

    public TableRow getTbrQues6() {
        return tbrQues6;
    }

    public TableRow getTbrQues7() {
        return tbrQues7;
    }

    public TableRow getTbrQues8() {
        return tbrQues8;
    }

    public TableRow getTbrQues9() {
        return tbrQues9;
    }

    public TableRow getTbrQues10() {
        return tbrQues10;
    }

    public TableRow getTbrQues11() {
        return tbrQues11;
    }

    public TableRow getTbrQues12() {
        return tbrQues12;
    }

    public TableRow getTbrQues13() {
        return tbrQues13;
    }

    public TableRow getTbrQues14() {
        return tbrQues14;
    }

    public TableRow getTbrQues15() {
        return tbrQues15;
    }

    public Button getBtnBoQua() {
        return btnBoQua;
    }
}
