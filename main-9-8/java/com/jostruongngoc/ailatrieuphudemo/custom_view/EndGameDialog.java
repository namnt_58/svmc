package com.jostruongngoc.ailatrieuphudemo.custom_view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.jostruongngoc.ailatrieuphudemo.R;
import com.jostruongngoc.ailatrieuphudemo.app.MainActivity;

/**
 * Created by Jos.TruongNgoc on 29-07-2016.
 */
public class EndGameDialog extends Dialog implements View.OnClickListener{
    private TextView tvSoCau, tvTienThuong, tvThanks;
    private Button btnThoat;
    private Context context;

    public EndGameDialog(Context context) {
        super(context);
        this.context = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.item_end_game_dialog);

        setCanceledOnTouchOutside(false);
        setCancelable(false);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().getAttributes().windowAnimations = R.style.LeftRightDialogAnimation;

        initViews();
    }

    private void initViews() {
        tvSoCau = (TextView) findViewById(R.id.tv_so_cau);
        tvTienThuong = (TextView) findViewById(R.id.tv_tien_thuong);
        btnThoat = (Button) findViewById(R.id.btn_thoat);
        btnThoat.setOnClickListener(this);

        tvThanks = (TextView) findViewById(R.id.tv_thanks);
    }

    @Override
    public void onClick(View view) {
        dismiss();
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    public TextView getTvSoCau() {
        return tvSoCau;
    }

    public TextView getTvTienThuong() {
        return tvTienThuong;
    }
}
