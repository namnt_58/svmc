package com.jostruongngoc.ailatrieuphudemo.custom_view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.jostruongngoc.ailatrieuphudemo.R;

/**
 * Created by Jos.TruongNgoc on 19-07-2016.
 */
public class DetailHelpCallDialog extends Dialog implements View.OnClickListener{
    private ImageView ivChuyenGia;
    private TextView tvChuyenGia, tvAnswer;
    private Button btnDong;

    public DetailHelpCallDialog(Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.item_detail_help_call);

        setCanceledOnTouchOutside(false);
        setCancelable(false);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().getAttributes().windowAnimations = R.style.LeftRightDialogAnimation;

        initViews();
    }

    private void initViews() {
        ivChuyenGia = (ImageView) findViewById(R.id.iv_chuyen_gia);
        tvChuyenGia = (TextView) findViewById(R.id.tv_chuyen_gia);
        tvAnswer = (TextView) findViewById(R.id.tv_answer);
        btnDong = (Button) findViewById(R.id.btn_dong);

        btnDong.setOnClickListener(this);
    }

    public ImageView getIvChuyenGia() {
        return ivChuyenGia;
    }

    public TextView getTvChuyenGia() {
        return tvChuyenGia;
    }

    public TextView getTvAnswer() {
        return tvAnswer;
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }
}
