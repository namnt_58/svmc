package com.jostruongngoc.ailatrieuphudemo.custom_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.jostruongngoc.ailatrieuphudemo.R;

/**
 * Created by Jos.TruongNgoc on 17-07-2016.
 */
public class ProgressView extends ImageView {

    public ProgressView(Context context) {
        super(context);
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAnim(attrs);
    }

    public ProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setAnim(attrs);
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        int max = Math.max(getMeasuredWidth(), getMeasuredHeight());
//        setMeasuredDimension(max, max);
//    }

    private void setAnim(AttributeSet attributeSet) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet, new int[] {R.attr.duration});
        int i = typedArray.getInt(0, 15000);
        typedArray.recycle();
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.anim_progress);
        loadAnimation.setDuration((long) i);
        startAnimation(loadAnimation);
    }
}
