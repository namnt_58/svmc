package com.jostruongngoc.ailatrieuphudemo.custom_view;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.jostruongngoc.ailatrieuphudemo.R;

/**
 * Created by Jos.TruongNgoc on 18-07-2016.
 */
public class DefaultDialog extends Dialog implements View.OnClickListener{
    private TextView tvContent;
    private Button btnKhong, btnCo;
    private Context mContext;

    public DefaultDialog(Context context) {
        super(context);
        mContext = context;
        getWindow().getAttributes().windowAnimations = R.style.LeftRightDialogAnimation;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.item_default_dialog);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        initViews();
    }

    private void initViews() {
        tvContent = (TextView) findViewById(R.id.tv_content);
        btnCo = (Button) findViewById(R.id.btn_co);
        btnKhong = (Button) findViewById(R.id.btn_khong);

        btnKhong.setOnClickListener(this);
        btnCo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_khong:
//                dismiss();
//                Intent intent = new Intent(mContext, MainActivity.class);
//                mContext.startActivity(intent);
                break;
            case R.id.btn_co:

                break;
        }
    }

    public void showDefaultDialog(String content) {
        tvContent.setText(content);
        show();
    }

    public Button getBtnKhong() {
        return btnKhong;
    }

    public Button getBtnCo() {
        return btnCo;
    }
}
