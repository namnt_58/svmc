package com.jostruongngoc.ailatrieuphudemo.custom_view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jostruongngoc.ailatrieuphudemo.R;

import java.util.Random;

/**
 * Created by sev_user on 8/4/2016.
 */
public class AudienceHelpDialog extends Dialog {
    private static final int HEIGHT_CONSTANT = 5;
    private Button btnCamOn;
    private ImageView ivAnswerA, ivAnswerB, ivAnswerC, ivAnswerD;
    private TextView tvPerA, tvPerB, tvPerC, tvPerD;
    private Animation animation;
    private int trueCase;
    private Context context;
    private int ans_per_a, ans_per_b, ans_per_c, ans_per_d;

    public AudienceHelpDialog(Context context) {
        super(context);
        this.context = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.item_audience_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().getAttributes().windowAnimations = R.style.LeftRightDialogAnimation;
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        initViews();
    }

    private void initViews() {
        btnCamOn = (Button) findViewById(R.id.btn_cam_on);

        ivAnswerA = (ImageView) findViewById(R.id.iv_answer_a);
        ivAnswerB = (ImageView) findViewById(R.id.iv_answer_b);
        ivAnswerC = (ImageView) findViewById(R.id.iv_answer_c);
        ivAnswerD = (ImageView) findViewById(R.id.iv_answer_d);
        tvPerA = (TextView) findViewById(R.id.tv_percent_a);
        tvPerB = (TextView) findViewById(R.id.tv_percent_b);
        tvPerC = (TextView) findViewById(R.id.tv_percent_c);
        tvPerD = (TextView) findViewById(R.id.tv_percent_d);
    }

    public void showAudienceHelpDialog(int trueCase) {
        this.trueCase = trueCase;
        show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                calculatePercentAnswer();
                changeHeight();
            }
        }, 3000);
    }

    private void changeHeight() {
        animation = AnimationUtils.loadAnimation(context, R.anim.anim_scale);

        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ans_per_a);
        params1.setMargins(0, 0, 0, 0);
        ivAnswerA.setLayoutParams(params1);
        ivAnswerA.startAnimation(animation);

        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ans_per_b);
        params2.setMargins(0, 0, 0, 0);
        ivAnswerB.setLayoutParams(params2);
        ivAnswerB.startAnimation(animation);

        RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ans_per_c);
        params3.setMargins(0, 0, 0, 0);
        ivAnswerC.setLayoutParams(params3);
        ivAnswerC.startAnimation(animation);

        RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ans_per_d);
        params4.setMargins(0, 0, 0, 0);
        ivAnswerD.setLayoutParams(params4);
        ivAnswerD.startAnimation(animation);
    }

    private void calculatePercentAnswer() {
        Random random = new Random();
        int a = random.nextInt(20) + 50;
        int b = random.nextInt(100 - a);
        int c = random.nextInt(100 - a - b);
        int d = 100 - a - b - c;

        if (trueCase == 1) {
            ans_per_a = HEIGHT_CONSTANT * a;
            ans_per_b = HEIGHT_CONSTANT * b;
            ans_per_c = HEIGHT_CONSTANT * c;
            ans_per_d = HEIGHT_CONSTANT * d;
        } else if (trueCase == 2) {
            ans_per_a = HEIGHT_CONSTANT * b;
            ans_per_b = HEIGHT_CONSTANT * a;
            ans_per_c = HEIGHT_CONSTANT * c;
            ans_per_d = HEIGHT_CONSTANT * d;
        } else if (trueCase == 3) {
            ans_per_a = HEIGHT_CONSTANT*c;
            ans_per_b = HEIGHT_CONSTANT*b;
            ans_per_c = HEIGHT_CONSTANT*a;
            ans_per_d = HEIGHT_CONSTANT*d;
        } else if (trueCase == 4) {
            ans_per_a = HEIGHT_CONSTANT*d;
            ans_per_b = HEIGHT_CONSTANT*b;
            ans_per_c = HEIGHT_CONSTANT*c;
            ans_per_d = HEIGHT_CONSTANT*a;
        }
        tvPerA.setText(ans_per_a/HEIGHT_CONSTANT +"%");
        tvPerB.setText(ans_per_b/HEIGHT_CONSTANT +"%");
        tvPerC.setText(ans_per_c/HEIGHT_CONSTANT +"%");
        tvPerD.setText(ans_per_d/HEIGHT_CONSTANT +"%");
    }


    public Button getBtnCamOn() {
        return btnCamOn;
    }
}
