package com.jostruongngoc.ailatrieuphudemo.custom_view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import com.jostruongngoc.ailatrieuphudemo.R;

/**
 * Created by Jos.TruongNgoc on 18-07-2016.
 */
public class HelpCallDialog extends Dialog implements View.OnClickListener{
    private ImageView ivBacSi, ivGiaoVien, ivKySu, ivPhongVien;
    private int trueCase;

    private DetailHelpCallDialog mDetailHelpCallDialog;
    private Context context;

    public HelpCallDialog(Context context) {
        super(context);
        this.context = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.item_help_call_dialog);

        setCanceledOnTouchOutside(false);
        setCancelable(false);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().getAttributes().windowAnimations = R.style.LeftRightDialogAnimation;

        initViews();
    }

    private void initViews() {
        mDetailHelpCallDialog = new DetailHelpCallDialog(context);
        ivBacSi = (ImageView) findViewById(R.id.iv_bac_si);
        ivGiaoVien = (ImageView) findViewById(R.id.iv_giao_vien);
        ivKySu = (ImageView) findViewById(R.id.iv_ky_su);
        ivPhongVien = (ImageView) findViewById(R.id.iv_phong_vien);

        ivBacSi.setOnClickListener(this);
        ivGiaoVien.setOnClickListener(this);
        ivKySu.setOnClickListener(this);
        ivPhongVien.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_bac_si:
                yKienChuyenGia(R.drawable.activity_player_layout_help_call_01, "Bác sĩ", trueCase);
                break;
            case R.id.iv_giao_vien:
                yKienChuyenGia(R.drawable.activity_player_layout_help_call_02, "Giáo viên", trueCase);
                break;
            case R.id.iv_ky_su:
                yKienChuyenGia(R.drawable.activity_player_layout_help_call_03, "Kỹ sư", trueCase);
                break;
            case R.id.iv_phong_vien:
                yKienChuyenGia(R.drawable.activity_player_layout_help_call_04, "Phóng viên", trueCase);
                break;
        }
    }

    private void yKienChuyenGia(int idImage, String chuyenGia, int trueCase) {
        dismiss();
        mDetailHelpCallDialog.getIvChuyenGia().setImageResource(idImage);
        mDetailHelpCallDialog.getTvChuyenGia().setText(chuyenGia);
        mDetailHelpCallDialog.getTvAnswer().setText(answer(trueCase));
        mDetailHelpCallDialog.show();
    }

    private String answer(int trueCase) {
        if (trueCase == 1) {
            return "Theo tôi đáp án đúng là A";
        } else if (trueCase == 2) {
            return "Theo tôi đáp án đúng là B";
        } else if (trueCase == 3) {
            return "Theo tôi đáp án đúng là C";
        }
        return "Theo tôi đáp án đúng là D";
    }


    public void showHelpCallDialog(int trueCase) {
        this.trueCase = trueCase;
        show();
    }

//    public ImageView getIvBacSi() {
//        return ivBacSi;
//    }

//    public ImageView getIvGiaoVien() {
//        return ivGiaoVien;
//    }
//
//    public ImageView getIvKySu() {
//        return ivKySu;
//    }
//
//    public ImageView getIvPhongVien() {
//        return ivPhongVien;
//    }
}
