package com.jostruongngoc.ailatrieuphudemo.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jostruongngoc.ailatrieuphudemo.R;
import com.jostruongngoc.ailatrieuphudemo.custom_view.AudienceHelpDialog;
import com.jostruongngoc.ailatrieuphudemo.custom_view.DefaultDialog;
import com.jostruongngoc.ailatrieuphudemo.custom_view.EndGameDialog;
import com.jostruongngoc.ailatrieuphudemo.custom_view.HelpCallDialog;
import com.jostruongngoc.ailatrieuphudemo.custom_view.MoneyDialog;
import com.jostruongngoc.ailatrieuphudemo.handing_data.DBManager;
import com.jostruongngoc.ailatrieuphudemo.handing_data.Question;

/**
 * Created by Jos.TruongNgoc on 17-07-2016.
 */
public class PlayGameActivity extends Activity implements View.OnClickListener, Runnable {
    private static final int CASE_A = 1;
    private static final int CASE_B = 2;
    private static final int CASE_C = 3;
    private static final int CASE_D = 4;
    //    private static final int UPDATE_QUESTION = 123456;
    private static final int UPDATE_TIME = 123457;
    private static final int SHOW_DIALOG_FULLTIME = 123458;
    private static final int SHOW_DIALOG_WRONG = 12345;
    private static final int UPDATE_SCORE = 123459;
    private static boolean CHOOSE = true;
    private boolean isPlaying = true;
    private static final String DUNG_CHOI = "BẠN ĐÃ DỪNG CHƠI Ở CÂU HỎI SỐ";
    private static final String SCORE[] = {
            "200,000", "400,000", "600,000", "1,000,000", "2,000,000", "3,000,000", "6,000,000", "10,000,000",
            "14,000,000", "22,000,000", "30,000,000", "40,000,000", "60,000,000", "85,000,000", "150,000,000"
    };

    private TextView tvTime, tvScore, tvLevel, tvQuestion, tvCaseA, tvCaseB, tvCaseC, tvCaseD;
    private ImageView iv5050, ivChangeQuestion, ivCall, ivHelpStop, ivAudience;

    private DBManager mDbManager;
    private DefaultDialog mDefaultDialog;
    private HelpCallDialog mHelpCallDialog;
    private EndGameDialog mEndGameDialog;
    private MoneyDialog mMoneyDialog;
    private AudienceHelpDialog mAudienceHelpDialog;
    private Thread thread;
    private int time, answer;
    private int trueCase = 0;
    private int level = 0;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE_SCORE:
                    tvScore.setText(SCORE[msg.arg1 - 1]);
                    break;
                case UPDATE_TIME:
                    tvTime.setText(msg.arg1 + "");
                    break;
                case SHOW_DIALOG_FULLTIME:
                    if (!isPlaying) return;
                    mDefaultDialog.showDefaultDialog(msg.obj.toString());
                    mDefaultDialog.getBtnKhong().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDefaultDialog.dismiss();
                            setViewEndGame();
                            mEndGameDialog.show();
                        }
                    });
                    mDefaultDialog.getBtnCo().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDefaultDialog.dismiss();
                            Intent intent = new Intent(PlayGameActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    break;
                case SHOW_DIALOG_WRONG:
                    mDefaultDialog.showDefaultDialog(msg.obj.toString());
                    mDefaultDialog.getBtnKhong().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDefaultDialog.dismiss();
                            setViewEndGame();
                            mEndGameDialog.show();
                            //  finish();
                        }
                    });
                    mDefaultDialog.getBtnCo().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDefaultDialog.dismiss();
                            Intent intent = new Intent(PlayGameActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    break;
            }
        }
    };

    private void setViewEndGame() {
        if (level <= 5) {
            mEndGameDialog.getTvSoCau().setText(DUNG_CHOI + " " + level);
            mEndGameDialog.getTvTienThuong().setText("0 VND");
        } else if (level <= 10) {
            mEndGameDialog.getTvSoCau().setText(DUNG_CHOI + " " + level);
            mEndGameDialog.getTvTienThuong().setText(SCORE[4] + " VND");
        } else {
            mEndGameDialog.getTvSoCau().setText(DUNG_CHOI + " " + level);
            mEndGameDialog.getTvTienThuong().setText(SCORE[9] + " VND");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);

        mDbManager = new DBManager(this);
        mDbManager.get15questions();
        initViews();

        thread = new Thread(this);
        thread.start();

        initQuestion();
    }

    private void initQuestion() {
        removeAllAnim();
        if (level < mDbManager.getArrQs().size()) {
            trueCase = mDbManager.getArrQs().get(level).getTrueCase();
            tvLevel.setText("Câu " + mDbManager.getArrQs().get(level).getLevel());
            tvQuestion.setText(mDbManager.getArrQs().get(level).getQuestion());
            tvCaseA.setText("A: " + mDbManager.getArrQs().get(level).getCaseA());
            tvCaseB.setText("B: " + mDbManager.getArrQs().get(level).getCaseB());
            tvCaseC.setText("C: " + mDbManager.getArrQs().get(level).getCaseC());
            tvCaseD.setText("D: " + mDbManager.getArrQs().get(level).getCaseD());
            level++;
        }
        mMoneyDialog.showQuestion(level);
        mMoneyDialog.show();
        mMoneyDialog.getBtnBoQua().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMoneyDialog.dismiss();
                CHOOSE = false;
                mMoneyDialog.removeQues();
                enableAllCase();
                enableHelp();
            }
        });
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mMoneyDialog.dismiss();
                CHOOSE = false;
                mMoneyDialog.removeQues();
                enableAllCase();
                enableHelp();
            }
        }, 4000);
    }

    private void enableHelp() {
        iv5050.setOnClickListener(this);
        ivAudience.setOnClickListener(this);
        ivHelpStop.setOnClickListener(this);
        ivCall.setOnClickListener(this);
        ivChangeQuestion.setOnClickListener(this);
    }

    private void removeAllAnim() {
        tvCaseA.setBackgroundResource(R.drawable.activity_player_layout_play_answer_background_normal);
        tvCaseA.setAnimation(null);
        tvCaseB.setBackgroundResource(R.drawable.activity_player_layout_play_answer_background_normal);
        tvCaseB.setAnimation(null);
        tvCaseC.setBackgroundResource(R.drawable.activity_player_layout_play_answer_background_normal);
        tvCaseC.setAnimation(null);
        tvCaseD.setBackgroundResource(R.drawable.activity_player_layout_play_answer_background_normal);
        tvCaseD.setAnimation(null);
    }

    private void enableAllCase() {
        tvCaseA.setEnabled(true);
        tvCaseB.setEnabled(true);
        tvCaseC.setEnabled(true);
        tvCaseD.setEnabled(true);
    }

    private void initViews() {
        mDefaultDialog = new DefaultDialog(this);
        mHelpCallDialog = new HelpCallDialog(this);
        mEndGameDialog = new EndGameDialog(this);
        mMoneyDialog = new MoneyDialog(this);
        mAudienceHelpDialog = new AudienceHelpDialog(this);
        tvCaseA = (TextView) findViewById(R.id.tv_case_a);
        tvCaseB = (TextView) findViewById(R.id.tv_case_b);
        tvCaseC = (TextView) findViewById(R.id.tv_case_c);
        tvCaseD = (TextView) findViewById(R.id.tv_case_d);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvScore = (TextView) findViewById(R.id.tv_score);
        tvLevel = (TextView) findViewById(R.id.tv_level);
        tvQuestion = (TextView) findViewById(R.id.tv_question);
        iv5050 = (ImageView) findViewById(R.id.iv_5050);
        ivChangeQuestion = (ImageView) findViewById(R.id.iv_change_question);
        ivCall = (ImageView) findViewById(R.id.iv_call);
        ivHelpStop = (ImageView) findViewById(R.id.iv_help_stop);
        ivAudience = (ImageView) findViewById(R.id.iv_audience);

        time = 30;
        answer = 0;

        tvTime.setText(time + "");
        tvScore.setText("0");

        tvCaseA.setOnClickListener(this);
        tvCaseB.setOnClickListener(this);
        tvCaseC.setOnClickListener(this);
        tvCaseD.setOnClickListener(this);
        iv5050.setOnClickListener(this);
        ivChangeQuestion.setOnClickListener(this);
        ivCall.setOnClickListener(this);
        ivHelpStop.setOnClickListener(this);
        ivAudience.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDbManager.closeDB();
        unregisterReceiver(bcCatchPlayChallange);
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(PlayGameActivity.this, "Hãy dừng cuộc chơi trước khi thoát!", Toast.LENGTH_LONG).show();
//        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_case_a:
                answer = CASE_A;
                CHOOSE = true;
                disableCase();
                disableHelp();
                showAnim(tvCaseA, R.drawable.activity_player_layout_play_answer_background_selected);
                delayedAnimAnswer(answer, trueCase, tvCaseA);
                break;
            case R.id.tv_case_b:
                answer = CASE_B;
                CHOOSE = true;
                disableCase();
                disableHelp();
                showAnim(tvCaseB, R.drawable.activity_player_layout_play_answer_background_selected);
                delayedAnimAnswer(answer, trueCase, tvCaseB);
                break;
            case R.id.tv_case_c:
                answer = CASE_C;
                CHOOSE = true;
                disableCase();
                disableHelp();
                showAnim(tvCaseC, R.drawable.activity_player_layout_play_answer_background_selected);
                delayedAnimAnswer(answer, trueCase, tvCaseC);
                break;
            case R.id.tv_case_d:
                answer = CASE_D;
                CHOOSE = true;
                disableCase();
                disableHelp();
                showAnim(tvCaseD, R.drawable.activity_player_layout_play_answer_background_selected);
                delayedAnimAnswer(answer, trueCase, tvCaseD);
                break;

            case R.id.iv_5050:
                iv5050.setEnabled(false);
                removeCase();
                break;
            case R.id.iv_change_question:
//                ivChangeQuestion.setEnabled(false);
//                changeQuestion();
                CHOOSE = true;
                mDefaultDialog.showDefaultDialog("Bạn có muốn đổi câu hỏi không?");
                mDefaultDialog.getBtnCo().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDefaultDialog.dismiss();
                        ivChangeQuestion.setEnabled(false);
                        CHOOSE = false;
                        changeQuestion();
                    }
                });
                mDefaultDialog.getBtnKhong().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDefaultDialog.dismiss();
                        CHOOSE = false;
                    }
                });
                break;
            case R.id.iv_call:
                ivCall.setEnabled(false);
                mHelpCallDialog.showHelpCallDialog(trueCase);
//                mHelpCallDialog.getIvBacSi().setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        mHelpCallDialog.dismiss();
//                    }
//                });
                break;
            case R.id.iv_help_stop:
                ivHelpStop.setEnabled(false);
                mDefaultDialog.showDefaultDialog("Bạn có muốn dừng cuộc chơi không?");
                mDefaultDialog.getBtnKhong().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDefaultDialog.dismiss();
                        ivHelpStop.setEnabled(true);
                    }
                });
                mDefaultDialog.getBtnCo().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CHOOSE = true;
                        mDefaultDialog.dismiss();
                        mEndGameDialog.getTvSoCau().setText(DUNG_CHOI + " " + level);
                        if (level >= 2) {
                            mEndGameDialog.getTvTienThuong().setText(SCORE[level - 2] + " VND");
                        } else {
                            mEndGameDialog.getTvTienThuong().setText("0 VND");
                        }
                        mEndGameDialog.show();
                    }
                });
                break;
            case R.id.iv_audience:
                CHOOSE = true;
                ivAudience.setEnabled(false);
                mAudienceHelpDialog.showAudienceHelpDialog(trueCase);
                mAudienceHelpDialog.getBtnCamOn().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAudienceHelpDialog.dismiss();
                        CHOOSE = false;
                    }
                });
                break;
        }
    }

    private void disableHelp() {
        iv5050.setOnClickListener(null);
        ivAudience.setOnClickListener(null);
        ivHelpStop.setOnClickListener(null);
        ivCall.setOnClickListener(null);
        ivChangeQuestion.setOnClickListener(null);
    }

    private void delayedAnimAnswer(final int answer, final int trueCase, final TextView tvCase) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (answer == trueCase) {
//                    tvCase.setBackgroundResource(R.drawable.activity_player_layout_play_answer_background_true);
//                    Animation animation = AnimationUtils.loadAnimation(PlayGameActivity.this,
//                            R.anim.anim_answer_choose);
//                    tvCase.startAnimation(animation);

                    showAnim(tvCase, R.drawable.activity_player_layout_play_answer_background_true);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            time = 30;
                            initQuestion();
                        }
                    }, 3000);
                    Message message = new Message();
                    message.what = UPDATE_SCORE;
                    message.arg1 = level;
                    message.setTarget(handler);
                    message.sendToTarget();
                } else {
                    isPlaying = false;
//                    tvCase.setBackgroundResource(R.drawable.activity_player_layout_play_answer_background_wrong);
//                    Animation animation = AnimationUtils.loadAnimation(PlayGameActivity.this,
//                            R.anim.anim_answer_choose);
//                    tvCase.startAnimation(animation);
                    showAnim(tvCase, R.drawable.activity_player_layout_play_answer_background_wrong);

                    showTrueAnswer(trueCase);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Message message = new Message();
                            message.what = SHOW_DIALOG_WRONG;
                            message.obj = "Bạn trả lời sai! Bạn có muốn chơi lại không?";
                            message.setTarget(handler);
                            message.sendToTarget();
                        }
                    }, 3000);
                }
            }
        }, 3000);
    }

    private void showAnim(TextView tvCase, int background) {
        tvCase.setBackgroundResource(background);
        Animation animation = AnimationUtils.loadAnimation(PlayGameActivity.this,
                R.anim.anim_answer_choose);
        tvCase.startAnimation(animation);
    }

    private void showTrueAnswer(int trueCase) {
        if (trueCase == CASE_A) {
//            tvCaseA.setBackgroundResource(R.drawable.activity_player_layout_play_answer_background_true);
//            Animation animation = AnimationUtils.loadAnimation(PlayGameActivity.this,
//                    R.anim.anim_answer_choose);
//            tvCaseA.startAnimation(animation);
            showAnim(tvCaseA, R.drawable.activity_player_layout_play_answer_background_true);
        } else if (trueCase == CASE_B) {
            showAnim(tvCaseB, R.drawable.activity_player_layout_play_answer_background_true);
        } else if (trueCase == CASE_C) {
            showAnim(tvCaseC, R.drawable.activity_player_layout_play_answer_background_true);
        } else {
            showAnim(tvCaseD, R.drawable.activity_player_layout_play_answer_background_true);
        }
    }

    private void disableCase() {
//        if (answer == CASE_A) {
//            tvCaseB.setEnabled(false);
//            tvCaseC.setEnabled(false);
//            tvCaseD.setEnabled(false);
//        } else if (answer == CASE_B) {
//            tvCaseA.setEnabled(false);
//            tvCaseC.setEnabled(false);
//            tvCaseD.setEnabled(false);
//        } else if (answer == CASE_C) {
//            tvCaseB.setEnabled(false);
//            tvCaseA.setEnabled(false);
//            tvCaseD.setEnabled(false);
//        } else {
//            tvCaseB.setEnabled(false);
//            tvCaseA.setEnabled(false);
//            tvCaseC.setEnabled(false);
//        }
        tvCaseA.setEnabled(false);
        tvCaseB.setEnabled(false);
        tvCaseC.setEnabled(false);
        tvCaseD.setEnabled(false);
    }


    private void changeQuestion() {
        Question tempQues = mDbManager.getOneQues(level);
        trueCase = tempQues.getTrueCase();
        tvLevel.setText("Câu " + tempQues.getLevel());
        tvQuestion.setText(tempQues.getQuestion());
        tvCaseA.setText("A: " + tempQues.getCaseA());
        tvCaseB.setText("B: " + tempQues.getCaseB());
        tvCaseC.setText("C: " + tempQues.getCaseC());
        tvCaseD.setText("D: " + tempQues.getCaseD());
    }

    private void removeCase() {
        if (trueCase == 1) {
            tvCaseC.setText("");
            tvCaseB.setText("");
        } else if (trueCase == 2) {
            tvCaseD.setText("");
            tvCaseC.setText("");
        } else if (trueCase == 3) {
            tvCaseA.setText("");
            tvCaseB.setText("");
        } else {
            tvCaseC.setText("");
            tvCaseA.setText("");
        }
    }

    @Override
    public void run() {
//        int count = 30;
        while (time >= 0 && isPlaying) {
            if (CHOOSE == false) {
//                if (level < mDbManager.getArrQs().size() && count == 30) {
//                    trueCase = mDbManager.getArrQs().get(level).getTrueCase();
//                    Message message = new Message();
//                    message.what = UPDATE_QUESTION;
//                    message.obj = mDbManager.getArrQs().get(level);
//                    message.setTarget(handler);
//                    message.sendToTarget();
//                    level++;
//                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//                count--;
//                if (count == 0) {
//                    count = 30;
//                }
                if (CHOOSE == false) {
                    Message message = new Message();
                    message.what = UPDATE_TIME;
                    message.arg1 = time;
                    message.setTarget(handler);
                    message.sendToTarget();
                    if (CHOOSE == false) {
                        time--;
                    }
                }
            }
//            if (answer == trueCase) {
//                CHOOSE = false;
////                count = 30;
//                time = 30;
//                answer = 0;
//            }
        }
        if (time < 0) {
//            Thread.currentThread().interrupt();
//            thread = null;

            Message message = new Message();
            message.what = SHOW_DIALOG_FULLTIME;
            message.obj = "Hết giờ! Bạn có muốn chơi lại không?";
            message.setTarget(handler);
            message.sendToTarget();
        }
    }

    @Override
    public void finish() {
        isPlaying = false;
        super.finish();
    }

    private BroadcastReceiver bcCatchPlayChallange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            thread.start();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BlueToothChat.ACTION_PLAY_GAME);
        registerReceiver(bcCatchPlayChallange, intentFilter);
    }


}
