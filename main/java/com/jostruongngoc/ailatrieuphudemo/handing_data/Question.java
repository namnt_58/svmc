package com.jostruongngoc.ailatrieuphudemo.handing_data;

import java.io.Serializable;

/**
 * Created by Jos.TruongNgoc on 18-07-2016.
 */
public class Question implements Serializable{
    private String question, caseA, caseB, caseC, caseD;
    private int level, trueCase;

    public Question() {

    }

    public Question(String question, String caseA, String caseB, String caseC, String caseD, int level, int trueCase) {
        this.question = question;
        this.caseA = caseA;
        this.caseB = caseB;
        this.caseC = caseC;
        this.caseD = caseD;
        this.level = level;
        this.trueCase = trueCase;
    }

    public String getQuestion() {
        return question;
    }

    public String getCaseA() {
        return caseA;
    }

    public String getCaseB() {
        return caseB;
    }

    public String getCaseC() {
        return caseC;
    }

    public String getCaseD() {
        return caseD;
    }

    public int getLevel() {
        return level;
    }

    public int getTrueCase() {
        return trueCase;
    }
}
